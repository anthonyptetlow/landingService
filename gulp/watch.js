var gulp = require('gulp');

gulp.task('watch', function () {
	gulp.watch(['./routes/**/*.js', './service/**/*.js', './middleware/**/*.js', '!./**/*.spec.js'], ['lint']);
});
