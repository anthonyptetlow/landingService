var gulp = require('gulp'),
	eslint = require('gulp-eslint'),
    debug = require('gulp-debug');


var eslintConf = {
        rulePaths: [],
        rules: {
            'quotes': [2, 'single'],
            'strict': 0,
            'global-strict': 0
        },
        envs: [
            'node'
        ]
    };


gulp.task('lint', function() {
	return gulp.src(['./routes/**/*.js', './service/**/*.js', './middleware/**/*.js', '!./**/*.spec.js'])
        // .pipe(debug())
        .pipe(eslint(eslintConf))
        .pipe(eslint.format())
		.on('error', function (error) {
            console.error(String(error));
        });
});
