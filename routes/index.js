var express = require('express'),
    LandingStore = require('../service/LandingStore');

var router = new express.Router();

router.get('/ping', function (req, res) {
	LandingStore.ping().then(function() {
		res.status(200).send();
	}, function (error) {
		res.status(500).send(error);
	});
});

router.post('/register', function (req, res) {
	LandingStore.addUser(req.body.email, req.body.projectKey)
	.then(function () {
		res.status(200).end();
	}, function (error) {
		res.status(500).send(error);
	});
});


module.exports = router;
