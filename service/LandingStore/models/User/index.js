var mongoose = require('mongoose');


module.exports = function (userDB) {

	function requiredStringValidator (query) {
		return typeof query === 'string' && query.length > 0;
	}

	var UserSchema = new mongoose.Schema({
		email: {type: String, unique: true, required: true },
		projectKeySet: {type: [String], required: true }
	});

	UserSchema.path('email').validate(requiredStringValidator, 'Invalid email');

	return userDB.model('User', UserSchema);
};
