var mongoose = require('mongoose'),
	q = require('q'),
	_ = require('lodash');

var mongoUri = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/LandingStore',
	userDB = mongoose.createConnection(mongoUri), // connect to our database
	User = require('./models/User')(userDB);

function ping() {
	var deferred = q.defer();
	deferred.resolve(true);
	return deferred.promise;
}

function addUser(email, projectKey) {
	return User.findOne({email: email}).then(function (user) {
		if (user) {
			if (!_.includes(user.projectKeySet, projectKey)) {
				user.projectKeySet.push(projectKey);
			}
		} else {
			user = new User({
				email: email,
				projectKeySet: [projectKey]
			});
		}
		return user.save();
	});
}

module.exports = {
	ping: ping,
	addUser: addUser
};
