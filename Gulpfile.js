var gulp = require('gulp');

require('./gulp/scripts');
require('./gulp/watch');

gulp.task('devLint', ['lint', 'watch']);
